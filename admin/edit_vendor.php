<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="../assets/layouts/layout/img/de-active/vendor.png" class="imgbasline"> Edit Vendor</div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_vendor" id="frm_vendor" action="vendor_list.php" class="horizontal-form" method="POST">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Vendor Code</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="vendor_code" id="vendor_code" placeholder="Vendor Code" value="28200">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Vendor Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="vendor_name" id="vendor_name" placeholder="Vendor Name" value="ABB LTD">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Email</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="email" id="email" placeholder="Email Address" value="abb@gmail.com">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Mobile</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile Number" value="9181882828">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Address</label>
                                <div class="col-md-8">
                                    <textarea type="text" class="form-control" name="address" id="address" placeholder="Address"  cols="5" rows="5">3rd Floor, Jayanth Tech Park, Old no7, New no 41, Mount Poonamalle High Rd, Chennai, Tamil Nadu 600098</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Status</label>
                                <div class="col-md-8">
                                    <div class="mt-radio-inline" style="padding: 2px 0;">
                                        <label class="mt-radio">
                                            <input type="radio" name="optionsRadios" id="optionsRadios25" value="option1" checked>Active
                                            <span></span>
                                        </label>
                                        <label class="mt-radio">
                                            <input type="radio" name="optionsRadios" id="optionsRadios26" value="option2"> Inactive
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <h3 class="form-section formheading">Add Products</h3>
                    <div class="row">
                        <div class="col-md-12 paddingleftright">
                            <div class="col-md-3 paddingbottom">
                                <div class="form-group">
                                    <label class="control-label col-md-12">Material Name</label>
                                    <div class="col-md-12">
                                        <select class="form-control" name="material_name" id="material_name">
                                            <option value="">Select Material</option>
                                            <option value="PG VANES 01480" selected>PG VANES 01480</option>
                                            <option value="SG-V-SEAL-59084">SG-V-SEAL-59084</option>
                                            <option value="DRIVE COUPLING">DRIVE COUPLING</option>
                                            <option value="BELL CUP D50">BELL CUP D50</option>
                                            <option value="PC COLLECTOR NOZZLE">PC COLLECTOR NOZZLE</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 paddingbottom">
                                <div class="form-group">
                                    <label class="control-label col-md-12">Part No</label>
                                    <div class="col-md-12">
                                        <select class="form-control" name="part_no" id="part_no">
                                            <option value="">Select Part No</option>
                                            <option value="NPMA0795">NPMA0795</option>
                                            <option value="NPMA3192" selected>NPMA3192</option>
                                            <option value="NPMA3195">NPMA3195</option>
                                            <option value="NPMA7134">NPMA7134</option>
                                            <option value="NPMA7135">NPMA7135</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 paddingbottom">
                                <div class="form-group">
                                    <label class="control-label col-md-12">Inward Type</label>
                                    <div class="col-md-12">
                                        <select class="form-control" name="inward_type" id="inward_type">
                                            <option value="">Select Inward Type</option>
                                            <option value="SPARES" selected>SPARES</option>
                                            <option value="CAPITAL">CAPITAL</option>
                                            <option value="CONSUM">CONSUM</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 paddingbottom paddingleftright">
                                <div class="form-group">
                                    <label class="control-label col-md-12">EUN (Unit)</label>
                                    <div class="col-md-12">
                                        <select class="form-control " name="part_eun" id="part_eun">
                                            <option value="">Select EUN Unit</option>
                                            <option value="NOS">NOS</option>
                                            <option value="PAC" selected>PAC</option>
                                            <option value="KG">KG</option>
                                            <option value="M">M</option>
                                            <option value="PC">PC</option>
                                            <option value="L">L</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1 paddingbottom text-center">
                                <label class="control-label col-md-12" style="color:#FFF">-</label>
                                <button type="button" class="btn btn-info customaddmorebtn" name="addmore" id="addmore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                            </div>
                        </div>
                        <!--/span-->
                        <div id="appendContent">
                        </div>
                    </div>
                </div>
                <div class="form-actions formbtncenter">
                    <button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save
                    </button>
                    <a href="vendor_list.php" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Cancel</a>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>
<script type="text/javascript">
    var i = 1;
$(document).on('click','#addmore', function() {
    var newfield ='<div class="col-md-12 paddingleftright"><div class="col-md-3 paddingbottom"><div class="form-group"><div class="col-md-12"><select class="form-control" name="material_name" id="material_name"><option value="">Select Material</option><option value="PG VANES 01480">PG VANES 01480</option><option value="SG-V-SEAL-59084">SG-V-SEAL-59084</option><option value="DRIVE COUPLING">DRIVE COUPLING</option><option value="BELL CUP D50">BELL CUP D50</option><option value="PC COLLECTOR NOZZLE">PC COLLECTOR NOZZLE</option></select></div></div></div><div class="col-md-3 paddingbottom"><div class="form-group"><div class="col-md-12"><select class="form-control" name="part_no" id="part_no"><option value="">Select Part No</option><option value="NPMA0795">NPMA0795</option><option value="NPMA3192">NPMA3192</option><option value="NPMA3195">NPMA3195</option><option value="NPMA7134">NPMA7134</option><option value="NPMA7135">NPMA7135</option></select></div></div></div><div class="col-md-3 paddingbottom"><div class="form-group"><div class="col-md-12"><select class="form-control" name="inward_type" id="inward_type"><option value="">Select Inward Type</option><option value="SPARES">SPARES</option><option value="CAPITAL">CAPITAL</option><option value="CONSUM">CONSUM</option></select></div></div></div><div class="col-md-2 paddingbottom paddingleftright"><div class="form-group"><div class="col-md-12"><select class="form-control " name="part_eun" id="part_eun"><option value="">Select EUN Unit</option><option value="NOS">NOS</option><option value="PAC">PAC</option><option value="KG">KG</option><option value="M">M</option><option value="PC">PC</option><option value="L">L</option></select></div></div></div><div class="col-md-1 paddingbottom"><div class="col-md-12"><button type="button" class="btn btn-info customremovemorebtn" name="remove" id="remove"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></div></div></div>';
    $('#appendContent').append(newfield);
    i++;
});
$(document).on('click','#remove', function(){
    $(this).parent().parent().parent('div').remove();
});
i++;
</script>