<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="../assets/layouts/layout/img/de-active/request.png" class="imgbasline"> Request List</div>
            <div class="actions">
              
            </div>
        </div>
        <div class="portlet-body">
	        <div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="emp_id" id="emp_id" placeholder="Employee Id">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="emp_name" id="emp_name" placeholder="Employee Name">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="area" id="area" placeholder="Area">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="sub_area" id="sub_area" placeholder="Sub Area">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="material_name" id="material_name" placeholder="Material Name">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="part_no" id="part_no" placeholder="Part No">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="enu_unit" id="enu_unit" placeholder="ENU (Unit)">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
	                        <select id="sel_status" name="sel_status" class="form-control">
	                        	<option value="">Select Status</option>
	                        	<option value="Approved">Approved</option>
	                        	<option value="Rejected">Rejected</option>
	                        </select>
                        </div>
	        		</div>
	        		<div class="col-md-12 text-center">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<a href="request_list.php" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
	        			</div>
	        		</div>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover" id="tblvendor">
	            	<thead>
	                    <tr>
	                        <th> SI.NO </th>
	                        <th> Emp Id </th>
	                        <th> Emp Name </th>
	                        <th> Area </th>
	                        <th> Sub Area </th>
	                        <th> Material Name </th>
	                        <th> Part No </th>
	                        <th> EUN </th>
	                        <th> Quantity </th>
	                        <th> Status </th>
	                    </tr>
	                </thead>
	                </tbody>
	                    <tr>
	                        <td> 1 </td>
	                        <td> EMP001 </td>
	                        <td> Naveen Kumar </td>
	                        <td> PS-1</td>
	                        <td> DUNK</td>
	                        <td> PC COLLECTOR NOZZLE </td>
	                        <td> NPMA0795 </td>
	                        <td> M</td>
	                        <td>100</td>
	                        <td><span class="label label-sm label-success labelboader"> Approved </span> </td>
	                    </tr>
	                    <tr>
	                        <td> 2 </td>
	                        <td> EMP002 </td>
	                        <td> Rajesh </td>
	                        <td> WTP</td>
	                        <td> RO PLANT</td>
	                        <td> BELL CUP D50 </td>
	                        <td> NPMA3192 </td>
	                        <td> PC</td>
	                        <td> 200</td>
	                        <td><span class="label label-sm label-danger labelboader"> Rejected </span> </td>
	                    </tr>
	                    <tr>
	                        <td> 3 </td>
	                        <td> EMP003 </td>
	                        <td> Sridhar </td>
	                        <td> FABSHOP</td>
	                        <td> SUBSTATION</td>
	                        <td> DRIVE COUPLING </td>
	                        <td> NPMA3195 </td>
	                        <td> KG</td>
	                        <td> 50</td>
	                        <td><span class="label label-sm label-danger labelboader"> Rejected </span> </td>
	                    </tr>
	                    <tr>
	                        <td> 4 </td>
	                        <td> EMP004 </td>
	                        <td> Ziaudeen </td>
	                        <td> COMMON</td>
	                        <td> CONSUMABLE</td>
	                        <td> SG-V-SEAL-59084 </td>
	                        <td> NPMA7134 </td>
	                        <td> PAC</td>
	                        <td> 80</td>
	                        <td><span class="label label-sm label-success labelboader"> Approved </span> </td>
	                    </tr>
	                    <tr>
	                        <td> 5 </td>
	                        <td> EMP005 </td>
	                        <td> Raja </td>
	                        <td> UTILITY</td>
	                        <td> SUBSTATION</td>
	                        <td> PG VANES 01480 </td>
	                        <td> NPMA7135 </td>
	                        <td> NOS</td>
	                        <td>79</td>
	                        <td><span class="label label-sm label-success labelboader"> Approved </span> </td>
	                    </tr>
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->

<?php 
include("footer.php"); 
?>

<script>
    $(document).ready(function() {
	$('#tblvendor').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":5 ,
        "ordering": false
    } );    
    } );
    $("#search_result_length").hide();
</script>