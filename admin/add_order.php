<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="../assets/layouts/layout/img/de-active/order.png" class="imgbasline">Purchase Order</div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_employee" id="frm_employee" action="order_list.php" class="horizontal-form" method="POST">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">PO Number</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="emp_name" id="emp_name" placeholder="Purchase Order Number" value="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">PO Date</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="po_date" id="po_date" placeholder="" value="">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Part No</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="emp_name" id="emp_name" placeholder="" value="NPMA0795">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Vendor Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="emp_name" id="emp_name" placeholder="" value="ANNUP">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Vendor Code</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="emp_name" id="emp_name" placeholder="" value="10609">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">EUN (Units)</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="emp_name" id="emp_name" placeholder="" value="Nos">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Quantity</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="emp_name" id="emp_name" placeholder="Purchase Quantity" value="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Inward Type</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="emp_name" id="emp_name" placeholder="" value="SPARES">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Status</label>
                                <div class="col-md-8">
                                    <div class="mt-radio-inline" style="padding: 2px 0;">
                                        <label class="mt-radio">
                                            <input type="radio" name="optionsRadios" id="optionsRadios25" value="option1" checked>Oreder Placed
                                            <span></span>
                                        </label>
                                        <label class="mt-radio">
                                            <input type="radio" name="optionsRadios" id="optionsRadios26" class="receivedOrder" value="option2"> Received
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    </div>
                    <!--/row-->
                <div class="form-actions formbtncenter">
                    <button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save
                    </button>
                    <a href="order_list.php" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Cancel</a>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<script>
$ (document).on("click",".receivedOrder",function (){
$("#PurchaseOrderID").modal("show");
});
$( function() {
    $("#po_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
});
</script>
<?php 
include("footer.php"); 
?>
