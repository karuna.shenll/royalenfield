<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="../assets/layouts/layout/img/de-active/store.png" class="imgbasline"> Inward List</div>
            <div class="actions">
                <!-- <a href="add_employee.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Employee</a> -->
            </div>
        </div>
        <div class="portlet-body">
	        <div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="material_name" id="material_name" placeholder="Material Name">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="part_no" id="part_no" placeholder="Part No">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="inward_type" id="inward_type" placeholder="Inward type">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="enu_unit" id="enu_unit" placeholder="ENU (UNIT)">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="inward_from" id="inward_from" placeholder="Inward From Date">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="inward_to" id="inward_to" placeholder="Inward To Date">
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<a href="inward_list.php" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
	        			</div>
	        		</div>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover" id="tblemployee">
	            	<thead>
	                    <tr>
	                        <th> SI.NO </th>
	                        <th> Inward Date</th>
	                        <th> Material Name</th>
	                        <th> Part Number </th>
	                        <th> Inward type </th>
	                        <th> EUN(Units)</th>
	                        <th> Quantity</th>   
	                    </tr>
	                </thead>
	                </tbody>
	                    <tr>
	                        <td> 1 </td>
	                        <td> 07/06/2018</td>
	                        <td> PG VANES 01480</td>
	                        <td> NPMA0795</td>
	                        <td> SPARES</td>
	                        <td> Nos</td>
	                        <td>10</td>   
	                    </tr>
	                    <tr>
	                        <td> 2 </td>
	                        <td> 08/6/2018 </td>
	                        <td>PG VANES 01480</td>
	                        <td> NPMA0795</td>
	                        <td> SPARES</td>
	                        <td> Nos</td>
	                        <td>15 </td>    
	                    </tr>
	                    <tr>
	                        <td> 3 </td>
	                        <td> 21/06/2018 </td>
	                        <td> PG VANES 01480</td>
	                        <td> NPMA0795</td>
	                        <td> SPARES</td>
	                        <td> Nos</td>
	                        <td>10</td>
	                    </tr>
	                    <tr>
	                        <td> 4 </td>
	                        <td> 22/06/2018 </td>
	                        <td>PG VANES 01480</td>
	                        <td> NPMA0795</td>
	                        <td>SPARES</td>
	                        <td> Nos</td>
	                        <td>10 </td>  
	                    </tr>
	                    <tr>
	                        <td> 5 </td>
	                        <td> 20/07/2018 </td>
	                        <td>PG VANES 01480</td>
	                        <td> NPMA0795</td>
	                        <td>SPARES</td>
	                        <td> Nos</td>
	                        <td>10 </td>
	                    </tr>
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>

<script>
    $(document).ready(function() {
	$('#tblemployee').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":5 ,
        "ordering": false
    } );    
    } );
    $("#search_result_length").hide();
    $( function() {
      $("#inward_from").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
    $( function() {
      $("#inward_to").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
</script>