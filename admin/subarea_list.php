<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="../assets/layouts/layout/img/de-active/area.png" class="imgbasline"> Subarea List</div>
            <div class="actions">
                <a href="add_subarea.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Subarea</a>
            </div>
        </div>
        <div class="portlet-body">
	        <div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        		 	<select class="form-control" name="area" id="area">
                            <option value="">Select Area</option>
                            <option value="PS-1">PS-1</option>
                            <option value="PS-2">PS-2</option>
                            <option value="FABSHOP">FABSHOP</option>
                            <option value="COMMON">COMMON</option>
                            <option value="UTILITY">UTILITY</option>
                        </select>
                    </div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="subarea_name" id="subarea_name" placeholder="Subarea">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
	                        <select id="sel_status" name="sel_status" class="form-control">
	                        	<option value="">Select Status</option>
	                        	<option value="Active">Active</option>
	                        	<option value="Inactive">Inactive</option>
	                        </select>
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<a href="subarea_list.php" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
	        			</div>
	        		</div>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover" id="tblarea">
	            	<thead>
	                    <tr>
	                        <th> SI.NO </th>
	                        <th> Area </th>
	                        <th> Sub Area </th>
	                        <th> Status </th>
	                        <th> Action </th>
	                    </tr>
	                </thead>
	                </tbody>
	                    <tr>
	                        <td> 1 </td>
	                        <td> PS-1 </td>
	                        <td> DUNK </td>
	                        <td><span class="label label-sm label-success labelboader"> Active </span> </td>
	                        <td> <a href="edit_subarea.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 2 </td>
	                        <td> WTP </td>
	                        <td> RO PLANT </td>
	                        <td><span class="label label-sm label-danger labelboader"> Inactive </span> </td>
	                        <td> <a href="edit_subarea.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 3 </td>
	                        <td> FABSHOP </td>
	                        <td> SUBSTATION </td>
	                        <td><span class="label label-sm label-success labelboader"> Active </span> </td>
	                        <td> <a href="edit_subarea.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 4 </td>
	                        <td> COMMON </td>
	                        <td>CONSUMABLE</td>
	                        <td><span class="label label-sm label-danger labelboader"> Inactive </span> </td>
	                        <td> <a href="edit_subarea.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 5 </td>
	                        <td> UTILITY </td>
	                        <td> SUBSTATION </td>
	                        <td><span class="label label-sm label-success labelboader"> Active </span> </td>
	                        <td> <a href="edit_subarea.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>

<script>
    $(document).ready(function() {
	$('#tblarea').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":5 ,
        "ordering": false
    } );    
    } );
    $("#search_result_length").hide();
</script>