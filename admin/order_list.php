<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="../assets/layouts/layout/img/de-active/order.png" class="imgbasline"> Purchase Order List</div>
            <div class="actions">
                <a href="add_order.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Order List</a>
            </div>
        </div>
        <div class="portlet-body">
	        <div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="driver_name" id="driver_name" placeholder="Material Name">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Part No">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="email" id="email" placeholder="Inward Type">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="unit" placeholder="EUN">
                        </div>
	        		</div>
	        		<div class="col-md-12 text-center">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<a href="order_list.php" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset </a>
	        			</div>
	        		</div>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover" id="tblemployee">
	            	<thead>
	                    <tr>
	                        <th> SI.NO </th>
	                        <th> Matrial Name </th>
	                        <th> Part No </th>
	                        <th> Inward Type</th>
	                        <th> EUN</th>
	                        <th> Status </th>
	                        <th> Action </th>
	                    </tr>
	                </thead>
	                </tbody>
	                    <tr>
	                        <td> 1 </td>
	                        <td> BOILER PH BOOSTER</td>
	                        <td> 211549 </td>
	                        <td> RO PLANT</td>
	                        <td> KG</td>
	                        <td><span class="label label-sm label-success labelboader orderReceivedId" style="cursor:pointer;"> Received </span></td>
	                        <td> <a href="edit_order.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 2 </td>
	                        <td> NUT-12MM </td>
	                        <td> NPMA3570</td>
	                        <td>SPARES</td>
	                        <td> NOS</td>
	                        <td><span class="label label-sm label-success labelboader orderReceivedId" style="cursor:pointer;"> Received </span></td>
	                        <td> <a href="edit_order.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 3 </td>
	                        <td> FT- GAS HOSE BLUE </td>
	                        <td>NPMA5788</td>
	                        <td> SPARES</td>
	                        <td> NOS</td>
	                        <td><span class="label label-sm label-info labelboader"> Order Placed </span></td>
	                        <td> <a href="edit_order.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 4 </td>
	                        <td>RTG IEC-SPACER </td>
	                        <td> NPMA3212</td>
	                        <td> SPARES</td>
	                        <td>NOS</td>
	                        <td><span class="label label-sm label-info labelboader"> Order Placed </span></td>
	                        <td> <a href="edit_order.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 5 </td>
	                        <td> NITRIC ACID </td>
	                        <td> 211303</td>
	                        <td> CONSUM.</td>
	                        <td> KG</td>
	                        <td><span class="label label-sm label-success labelboader orderReceivedId" style="cursor:pointer;"> Received </span></td>
	                        <td> <a href="edit_order.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<div class="modal fade in" id="PurchaseOrderID" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="tender_cancel" id="tender_cancel" method="POST">
                <input type="hidden" name="hnd_id" id="hnd_id">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><b>Purchase Order</b></h4>
                </div>
                <div class="modal-body">
                    <table class="table table-striped table-bordered table-hover dataTable no-footer" id="orderid">
                        <thead>
                            <tr>
                                <th> SI.NO </th>
                                <th> Matrial Name </th>
                                <th> Part No </th>
                                <th> Inward Type</th>
                                <th> EUN</th>
                                <th> Action</th>
                            </tr>
                        </thead>
                        </tbody>
                            <tr>
                                <td> 1 </td>
                                <td> BOILER PH BOOSTER</td>
                                <td> 211549 </td>
                                <td> RO PLANT</td>
                                <td> KG</td>
                                <td>
                                    <input type="checkbox" name="gender" id="male" value="male">
                                </td>
                            </tr>
                            <tr>
                                <td> 2 </td>
                                <td> NUT-12MM </td>
                                <td> NPMA3570</td>
                                <td>SPARES</td>
                                <td> NOS</td>
                                <td>
                                    <input type="checkbox" name="spares" id="nut" value="nut">
                                </td>
                            </tr>
                            <tr>
                                <td> 3 </td>
                                <td> FT- GAS HOSE BLUE </td>
                                <td>NPMA5788</td>
                                <td> SPARES</td>
                                <td> NOS</td>
                                <td>
                                    <input type="checkbox" name="spares" id="nut" value="nut">
                                </td>
                            </tr>
                            <tr>
                                <td> 4 </td>
                                <td>RTG IEC-SPACER </td>
                                <td> NPMA3212</td>
                                <td> SPARES</td>
                                <td>NOS</td>
                                <td>
                                    <input type="checkbox" name="spares" id="nut" value="nut">
                                </td>
                            </tr>
                            <tr>
                                <td> 5 </td>
                                <td> NITRIC ACID </td>
                                <td> 211303</td>
                                <td> CONSUM.</td>
                                <td> KG</td>
                                <td>
                                    <input type="checkbox" name="spares" id="nut" value="nut">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer" id="modal-footer">
                    <button type="button" class="btn green customsavebtn" data-dismiss="modal"> <i class="fa fa-check"></i> Accepted</button>
                    <button type="button" class="btn red customrestbtn" data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
                </div>
            </form>
        <!-- /.modal-content -->
        </div>
    <!-- /.modal-dialog -->
    </div>
</div>
<?php 
include("footer.php"); 
?>

<script>
    $(document).ready(function() {
	$('#tblemployee').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":5 ,
        "ordering": false
    } );    
    } );
    $("#search_result_length").hide();

	$ (document).on("click",".orderReceivedId",function (){
		$("#PurchaseOrderID").modal("show");
	});
    $( function() {
      $("#from_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
    $( function() {
      $("#to_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
     $( function() {
      $("#expire_from").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
    $( function() {
      $("#expire_to").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
</script>