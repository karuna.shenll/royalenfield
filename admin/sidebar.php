<?php 
error_reporting(0);
$navigationURL = reset(explode("?",end(explode("/",$_SERVER["REQUEST_URI"]))));
?>
<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 0px">
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <?php
                if (strtolower($_SESSION["userrole"]) == "admin") {
            ?>
            <!-- END SIDEBAR TOGGLER BUTTON -->
            <li class="nav-item <?php echo ($navigationURL=='dashboard.php')?'active open':''; ?>">
                <a href="dashboard.php" class="nav-link nav-toggle">
                    <i class="dashboard-image"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                </a>
            </li>

            <li class="nav-item <?php echo ($navigationURL=='employee_list.php' || $navigationURL=='add_employee.php' || $navigationURL=='edit_employee.php')?'active open':''; ?>">
                <a href="employee_list.php" class="nav-link nav-toggle">
                    <i class="icon-employee"></i>
                    <span class="title">Manage Employee</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item  <?php echo ($navigationURL=='vendor_list.php' || $navigationURL=='add_vendor.php' ||  $navigationURL=='edit_vendor.php')?'active open':''; ?>">
                <a href="vendor_list.php" class="nav-link nav-toggle">
                    <i class="icon-vendor"></i>
                    <span class="title">Manage Vendors</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item  <?php echo ($navigationURL=='material_list.php' || $navigationURL=='add_material.php' || $navigationURL=='edit_material.php')?'active open':''; ?>">
                <a href="material_list.php" class="nav-link nav-toggle">
                    <i class="icon-material"></i>
                    <span class="title">Manage Material</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item <?php echo ($navigationURL=='area_list.php' || $navigationURL=='add_area.php' || $navigationURL=='edit_area.php' || $navigationURL=='subarea_list.php' || $navigationURL=='add_subarea.php' || $navigationURL=='edit_subarea.php')?'active open':''; ?>">
                <a href="javascript:void(0);" class="nav-link nav-toggle">
                    <i class="icon-area"></i>
                    <span class="title">Manage Area</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start <?php echo ($navigationURL=='area_list.php' || $navigationURL=='add_area.php' || $navigationURL=='area_list.php')?'active open':''; ?>">
                        <a href="area_list.php" class="nav-link">
                            <span class="title">Area</span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo ($navigationURL=='subarea_list.php' || $navigationURL=='add_subarea.php' || $navigationURL=='subarea_list.php')?'active open':''; ?>">
                        <a href="subarea_list.php" class="nav-link">
                            <span class="title">Subarea</span>
                        </a>
                    </li>
                </ul>
            </li>
             <li class="nav-item <?php echo ($navigationURL=='order_list.php' || $navigationURL=='add_order.php' || $navigationURL=='edit_order.php')?'active open':''; ?>">
                <a href="order_list.php" class="nav-link nav-toggle">
                    <i class="icon-order"></i>
                    <span class="title">Manage Purchase Order</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item <?php echo ($navigationURL=='company_list.php' || $navigationURL=='add_company.php' || $navigationURL=='edit_company.php' || $navigationURL=='inward_list.php' || $navigationURL=='outward_list.php' || $navigationURL=='edit_trip.php' )?'active open':''; ?>">
                <a href="javascript:void(0);" class="nav-link nav-toggle">
                    <i class="icon-store"></i> 
                    <span class="title">Manage Store</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start <?php echo ($navigationURL=='inward_list.php')?'active open':''; ?>">
                        <a href="inward_list.php" class="nav-link">
                            <span class="title">Inward</span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo ($navigationURL=='outward_list.php')?'active open':''; ?>">
                        <a href="outward_list.php" class="nav-link">
                            <span class="title">Outward</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item <?php echo ($navigationURL=='request_list.php')?'active open':''; ?>">
                <a href="request_list.php" class="nav-link nav-toggle">
                    <i class="icon-request"></i>
                    <span class="title">Manage Request</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item <?php echo ($navigationURL=='reports_list.php')?'active open':''; ?>">
                <a href="reports_list.php" class="nav-link nav-toggle">
                    <i class="icon-reports"></i>
                    <span class="title">Manage Reports</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item <?php echo ($navigationURL=='role_list.php' || $navigationURL=='add_role.php' || $navigationURL=='edit_role.php')?'active open':''; ?>">
                <a href="javascript:void(0);" class="nav-link nav-toggle">
                    <i class="icon-setting"></i>
                    <span class="title">Manage Settings</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start <?php echo ($navigationURL=='role_list.php' || $navigationURL=='add_role.php' || $navigationURL=='edit_role.php')?'active open':''; ?>">
                        <a href="role_list.php" class="nav-link">
                            <span class="title">Roles</span>
                        </a>
                    </li>
                </ul>
            </li>
            <?php
            }
            ?>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->