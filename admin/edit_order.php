<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="../assets/layouts/layout/img/de-active/order.png" class="imgbasline">Purchase Order</div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_employee" id="frm_employee" action="order_list.php" class="horizontal-form" method="POST">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">PO Number</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="emp_name" id="emp_name" placeholder="Purchase Order Number" value="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">PO Date</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="emp_name" id="emp_name" placeholder="Purchase Order Date" value="">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Part No</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="emp_name" id="emp_name" placeholder="" value="NPMA0795">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Vendor Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="emp_name" id="emp_name" placeholder="" value="ANNUP">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Vendor Code</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="emp_name" id="emp_name" placeholder="" value="10609">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">EUN (Units)</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="emp_name" id="emp_name" placeholder="" value="Nos">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Quantity</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="emp_name" id="emp_name" placeholder="Purchase Quantity" value="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Inward Type</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="emp_name" id="emp_name" placeholder="" value="SPARES">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Status</label>
                                <div class="col-md-8">
                                    <div class="mt-radio-inline" style="padding: 2px 0;">
                                        <label class="mt-radio">
                                            <input type="radio" name="optionsRadios" id="optionsRadios25" value="option1" checked>Oreder Placed
                                            <span></span>
                                        </label>
                                        <label class="mt-radio">
                                            <input type="radio" name="optionsRadios" id="optionsRadios26" class="receivedOrder" value="option2"> Received
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    </div>
                    <!--/row-->
                <div class="form-actions formbtncenter">
                    <button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save
                    </button>
                    <a href="order_list.php" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Cancel</a>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<div class="modal fade in" id="PurchaseOrderID" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="tender_cancel" id="tender_cancel" method="POST">
                <input type="hidden" name="hnd_id" id="hnd_id">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><b>Purchase Order</b></h4>
                </div>
                <div class="modal-body">
                    <table class="tabl table-border table-hover">
                        <thead>
                            <tr>
                                <th> SI.NO </th>
                                <th> Matrial Name </th>
                                <th> Part No </th>
                                <th> Inward Type</th>
                                <th> EUN</th>
                            </tr>
                        </thead>
                        </tbody>
                            <tr>
                                <td> 1 </td>
                                <td> BOILER PH BOOSTER</td>
                                <td> 211549 </td>
                                <td> RO PLANT</td>
                                <td> KG</td>
                                <td>
                                    <input type="checkbox" name="gender" id="male" value="male">
                                </td>
                            </tr>
                            <tr>
                                <td> 2 </td>
                                <td> NUT-12MM </td>
                                <td> NPMA3570</td>
                                <td>SPARES</td>
                                <td> NOS</td>
                                <td>
                                    <input type="checkbox" name="spares" id="nut" value="nut">
                                </td>
                            </tr>
                            <tr>
                                <td> 3 </td>
                                <td> FT- GAS HOSE BLUE </td>
                                <td>NPMA5788</td>
                                <td> SPARES</td>
                                <td> NOS</td>
                                <td>
                                    <input type="checkbox" name="spares" id="nut" value="nut">
                                </td>
                            </tr>
                            <tr>
                                <td> 4 </td>
                                <td>RTG IEC-SPACER </td>
                                <td> NPMA3212</td>
                                <td> SPARES</td>
                                <td>NOS</td>
                                <td>
                                    <label for="nut">NUT-12MM </label>
                                    <input type="checkbox" name="spares" id="nut" value="nut">
                                </td>
                            </tr>
                            <tr>
                                <td> 5 </td>
                                <td> NITRIC ACID </td>
                                <td> 211303</td>
                                <td> CONSUM.</td>
                                <td> KG</td>
                                <td>
                                    <label for="nut">NUT-12MM </label>
                                    <input type="checkbox" name="spares" id="nut" value="nut">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer" id="modal-footer">
                    <button type="button" class="btn green customsavebtn" data-dismiss="modal"> <i class="fa fa-check"></i> Accepted</button>
                    <button type="button" class="btn red customrestbtn" data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
                </div>
            </form>
        <!-- /.modal-content -->
        </div>
    <!-- /.modal-dialog -->
    </div>
</div>
<!-- END CONTENT BODY -->
<script>
$ (document).on("click",".receivedOrder",function (){
$("#PurchaseOrderID").modal("show");
});
</script>
<?php 
include("footer.php"); 
?>
