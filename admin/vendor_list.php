<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="../assets/layouts/layout/img/de-active/vendor.png" class="imgbasline"> Vendors List</div>
            <div class="actions">
                <a href="add_vendor.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Vendor</a>
            </div>
        </div>
        <div class="portlet-body">
	        <div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="vendor_code" id="vendor_code" placeholder="Vendor Code">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="vendor_name" id="vendor_name" placeholder="Vendor Name">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="email" id="email" placeholder="Email Address">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
	                        <select id="sel_status" name="sel_status" class="form-control">
	                        	<option value="">Select Status</option>
	                        	<option value="Active">Active</option>
	                        	<option value="Inactive">Inactive</option>
	                        </select>
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<a href="vendor_list.php" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
	        			</div>
	        		</div>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover" id="tblvendor">
	            	<thead>
	                    <tr>
	                        <th> SI.NO </th>
	                        <th> Vendor Code </th>
	                        <th> Vendor Name </th>
	                        <th> Mobile </th>
	                        <th> Email </th>
	                        <th> Status </th>
	                        <th> Action </th>
	                    </tr>
	                </thead>
	                </tbody>
	                    <tr>
	                        <td> 1 </td>
	                        <td> 10609 </td>
	                        <td> ABB LTD </td>
	                        <td> 9876545630</td>
	                        <td> abb@gmail.com</td>
	                        <td><span class="label label-sm label-success labelboader"> Active </span> </td>
	                        <td> <a href="edit_vendor.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 2 </td>
	                        <td> 13423 </td>
	                        <td> RITESTAR TECHNOLOGY </td>
	                        <td> 9876545631</td>
	                        <td> ritestar.tech@gmail.com</td>
	                        <td><span class="label label-sm label-danger labelboader"> Inactive </span> </td>
	                        <td> <a href="edit_vendor.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 3 </td>
	                        <td> 28200 </td>
	                        <td> HELUKABEL INDIA PVT LTD </td>
	                        <td> 9876545632</td>
	                        <td> helukabel@gmail.com</td>
	                        <td><span class="label label-sm label-success labelboader"> Active </span> </td>
	                        <td> <a href="edit_vendor.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 4 </td>
	                        <td> 13219 </td>
	                        <td> FUSION ENGINEERING </td>
	                        <td> 9876545633</td>
	                        <td> fusion@gmail.com</td>
	                        <td><span class="label label-sm label-danger labelboader"> Inactive </span> </td>
	                        <td> <a href="edit_vendor.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 5 </td>
	                        <td> 13133 </td>
	                        <td> SHIVAM ENGINEERING COMPANY </td>
	                        <td> 9876545634</td>
	                        <td> shivameng@gmail.com</td>
	                        <td><span class="label label-sm label-success labelboader"> Active </span> </td>
	                        <td> <a href="edit_vendor.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>

<script>
    $(document).ready(function() {
	$('#tblvendor').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":5 ,
        "ordering": false
    } );    
    } );
    $("#search_result_length").hide();
</script>