<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content"  id="dashboard">
	<div class="row widget-row">
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb dashboard-stat red text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">No Of Employee</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-white icon-social-dropbox redicon dashboardborder"></i>
                    <div class="widget-thumb-body">
                        <!-- <span class="widget-thumb-subtitle">USD</span> -->
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="25">25</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb dashboard-stat bg-green-jungle text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">No Of Vendors</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-white fa fa-users font-green-jungle dashboardborder"></i>
                    <div class="widget-thumb-body">
                        <!-- <span class="widget-thumb-subtitle">USD</span> -->
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="15">15</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb dashboard-stat yellow-casablanca text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">No Of Purchase Order</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-white fa fa-car dashboardborder font-yellow-casablanca"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="20">20</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb dashboard-stat blue text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">No of Request</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-white font-blue icon-users dashboardborder"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="07">07</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-share font-dark hide"></i>
                        <span class="caption-subject font-dark bold uppercase">Recent Activities</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="slimScrollDiv"><div class="scroller" style="height: 300px;">
                        <ul class="feeds">
                            <li>
                                <div class="col1">
                                    <div class="cont">
                                        <div class="cont-col1">
                                            <div class="label label-sm label-info">
                                                <i class="fa fa-check"></i>
                                            </div>
                                        </div>
                                        <div class="cont-col2">
                                            <div class="desc"> You have requested <b>NUT-12MM</b>.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col2">
                                    <div class="date"> Just now </div>
                                </div>
                            </li>
                            <li>
                                <div class="col1">
                                    <div class="cont">
                                        <div class="cont-col1">
                                            <div class="label label-sm label-info">
                                                <i class="fa fa-check"></i>
                                            </div>
                                        </div>
                                        <div class="cont-col2">
                                            <div class="desc"> You have requested <b>FT- GAS HOSE BLUE</b>.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col2">
                                    <div class="date">  10 mins  </div>
                                </div>
                            </li>
                            <li>
                                <div class="col1">
                                    <div class="cont">
                                        <div class="cont-col1">
                                            <div class="label label-sm label-info">
                                                <i class="fa fa-check"></i>
                                            </div>
                                        </div>
                                        <div class="cont-col2">
                                            <div class="desc"> You have requested <b>RTG IEC-SPACER</b>.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col2">
                                    <div class="date">  15 mins </div>
                                </div>
                            </li>
                            <li>
                                <div class="col1">
                                    <div class="cont">
                                        <div class="cont-col1">
                                            <div class="label label-sm label-info">
                                                <i class="fa fa-check"></i>
                                            </div>
                                        </div>
                                        <div class="cont-col2">
                                            <div class="desc"> You have requested <b>NITRIC ACID</b>.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col2">
                                    <div class="date">  20 mins  </div>
                                </div>
                            </li>
                            <li>
                                <div class="col1">
                                    <div class="cont">
                                        <div class="cont-col1">
                                            <div class="label label-sm label-info">
                                                <i class="fa fa-check"></i>
                                            </div>
                                        </div>
                                        <div class="cont-col2">
                                            <div class="desc"> You have requested <b>boiler ph booster</b>.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col2">
                                    <div class="date"> 2 hours  </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>