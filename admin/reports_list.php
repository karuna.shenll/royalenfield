<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="../assets/layouts/layout/img/de-active/reports.png" class="imgbasline"> Report List</div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">
	        <div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="part_no" id="part_no" placeholder="Part No">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="material_name" id="material_name" placeholder="Material Name">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="purcahse_from" id="purcahse_from" placeholder="Purchase From Date">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="purcahse_to" id="purcahse_to" placeholder="Purchase To Date">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="inward_from" id="inward_from" placeholder="Inward From Date">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="inward_to" id="inward_to" placeholder="Inward To Date">
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<a href="reports_list.php" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
	        			</div>
	        		</div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-12">
	        		<div class="col-md-3 co-sm-6 col-xs-12 totalcount">
	        		   <br>
                       <span> <b>Stack Quantity : 500 </b></span><br>
                       <span> <b>Total Quantity : 300 </b></span>
                       <br>
                       <br>
	        		</div>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover" id="tblvendor">
	            	<thead>
	                    <tr>
	                        <th> SI.NO </th>
	                        <th> Part No </th>
	                        <th> Material Name </th>
	                        <th> Purchase Date </th>
	                        <th> Purchase Quantity </th>
	                        <th> Inward Date </th>
	                        <th> Inward Quantity</th>
	                    </tr>
	                </thead>
	                </tbody>
	                    <tr>
	                        <td> 1 </td>
	                        <td> NPMA0795 </td>
	                        <td>PG VANES 01480</td>
	                        <td>26/07/2018</td>
	                        <td>50</td>
	                        <td>25/07/2018</td>
	                        <td>50</td>
	                    </tr>
	                    <tr>
	                        <td> 2 </td>
	                        <td> NPMA3195 </td>
	                        <td> TOP BEARING P.NO;56052 </td>
	                        <td> 27/07/2018</td>
	                        <td> 20</td>
	                        <td> 27/07/2018 </td>
	                        <td> 15 </td>
	                    </tr>
	                    <tr>
	                        <td> 3 </td>
	                        <td> NPMA3192 </td>
	                        <td> BELL CUP D50</td>
	                        <td> 28/07/2018</td>
	                        <td> 30</td>
	                        <td> 28/07/2018 </td>
	                        <td> 30 </td>
	                    </tr>
	                    <tr>
	                        <td> 4 </td>
	                        <td> NPMA3380 </td>
	                        <td> ROTOR/BLADE SET P.NO;57113 </td>
	                        <td> 29/07/2018</td>
	                        <td> 40 </td>
	                        <td> 29/07/2018</td>
	                        <td> 30 </td>
	                    </tr>
	                    <tr>
	                        <td> 5 </td>
	                        <td> NPMA7593 </td>
	                        <td> BELL CUP D50 </td>
	                        <td> 30/07/2018</td>
	                        <td> 20 </td>
	                        <td> 30/07/2018</td>
	                        <td> 15 </td>
	                    </tr>
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>

<script>
    $(document).ready(function() {
	$('#tblvendor').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":5 ,
        "ordering": false
    } );    
    } );
    $("#search_result_length").hide();
    $( function() {
      $("#purcahse_from").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
    $( function() {
      $("#purcahse_to").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
    $( function() {
      $("#inward_from").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
    $( function() {
      $("#inward_to").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
</script>