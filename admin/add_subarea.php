<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="../assets/layouts/layout/img/de-active/area.png" class="imgbasline"> Add Subarea</div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_subarea" id="frm_subarea" action="subarea_list.php" class="horizontal-form" method="POST">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Area</label>
                                <div class="col-md-8">
                                    <select class="form-control" name="area" id="area">
                                        <option value="">Select Area</option>
                                        <option value="PS-1">PS-1</option>
                                        <option value="PS-2">PS-2</option>
                                        <option value="FABSHOP">FABSHOP</option>
                                        <option value="COMMON">COMMON</option>
                                        <option value="UTILITY">UTILITY</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Sub Area</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="subarea_name" id="subarea_name" placeholder="Sub Area" value="">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                         <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Status</label>
                                <div class="col-md-8">
                                    <div class="mt-radio-inline" style="padding: 2px 0;">
                                        <label class="mt-radio">
                                            <input type="radio" name="optionsRadios" id="optionsRadios25" value="option1" checked>Active
                                            <span></span>
                                        </label>
                                        <label class="mt-radio">
                                            <input type="radio" name="optionsRadios" id="optionsRadios26" value="option2"> Inactive
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                        </div>
                    </div>
                    <!--/row-->
                </div>
                <div class="form-actions formbtncenter">
                	<button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save
                    </button>
                    <a href="subarea_list.php" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Cancel</a>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>