<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="../assets/layouts/layout/img/de-active/setting.png" class="imgbasline"> Edit Role</div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_role" id="frm_role" action="role_list.php" class="horizontal-form" method="POST">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Role Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="role_name" id="role_name" placeholder="Role Name" value="Employee">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Status</label>
                                <div class="col-md-8">
                                    <div class="mt-radio-inline" style="padding: 2px 0;">
                                        <label class="mt-radio">
                                            <input type="radio" name="optionsRadios" id="optionsRadios25" value="option1" checked>Active
                                            <span></span>
                                        </label>
                                        <label class="mt-radio">
                                            <input type="radio" name="optionsRadios" id="optionsRadios26" value="option2"> Inactive
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                </div>
                <div class="form-actions formbtncenter">
                    <button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save
                    </button>
                    <a href="role_list.php" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Cancel</a>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>