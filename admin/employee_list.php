<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="../assets/layouts/layout/img/de-active/employee.png" class="imgbasline"> Employee List</div>
            <div class="actions">
                <a href="add_employee.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Employee</a>
            </div>
        </div>
        <div class="portlet-body">
	        <div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="employee_name" id="employee_name" placeholder="Employee Name">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="email" id="email" placeholder="Email Address">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
	                        <select class="form-control" name="role" id="role">
                        		<option value="">Select Role</option>
                        		<option value="Supervisor">Supervisor</option>
                        		<option value="Employee">Employee</option>
                        		<option value="Store Keeper">Store Keeper</option>
                        	</select>
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
	                        <select id="sel_status" name="sel_status" class="form-control">
	                        	<option value="">Select Status</option>
	                        	<option value="Active">Active</option>
	                        	<option value="Inactive">Inactive</option>
	                        </select>
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<a href="employee_list.php" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
	        			</div>
	        		</div>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover" id="tblemployee">
	            	<thead>
	                    <tr>
	                        <th> SI.NO </th>
	                        <th> Emp Id </th>
	                        <th> Emp Name </th>
	                        <th> Mobile </th>
	                        <th> Email </th>
	                        <th> Role</th>
	                        <th> Status </th>
	                        <th> Action </th>
	                    </tr>
	                </thead>
	                </tbody>
	                    <tr>
	                        <td> 1 </td>
	                        <td> EMP001 </td>
	                        <td> Naveen Kumar </td>
	                        <td> 9876545630</td>
	                        <td> naveenkumar@gmail.com</td>
	                        <td> Employee</td>
	                        <td><span class="label label-sm label-success labelboader"> Active </span> </td>
	                        <td> <a href="edit_employee.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 2 </td>
	                        <td> EMP002 </td>
	                        <td> Rajesh </td>
	                        <td> 9876545631</td>
	                        <td> rajesh@gmail.com</td>
	                        <td> Supervisor</td>
	                        <td><span class="label label-sm label-danger labelboader"> Inactive </span> </td>
	                        <td> <a href="edit_employee.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 3 </td>
	                        <td> EMP003 </td>
	                        <td> Sridhar </td>
	                        <td> 9876545632</td>
	                        <td> Sridhar@gmail.com</td>
	                        <td> Employee</td>
	                        <td><span class="label label-sm label-success labelboader"> Active </span> </td>
	                        <td> <a href="edit_employee.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 4 </td>
	                        <td> EMP004 </td>
	                        <td> Ziaudeen </td>
	                        <td> 9876545633</td>
	                        <td> ziaudeen@gmail.com</td>
	                        <td> Store Keeper</td>
	                        <td><span class="label label-sm label-danger labelboader"> Inactive </span> </td>
	                        <td> <a href="edit_employee.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 5 </td>
	                        <td> EMP005 </td>
	                        <td> Raja </td>
	                        <td> 9876545634</td>
	                        <td> raja@gmail.com</td>
	                        <td> Store Keeper</td>
	                        <td><span class="label label-sm label-success labelboader"> Active </span> </td>
	                        <td> <a href="edit_employee.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>

<script>
    $(document).ready(function() {
	$('#tblemployee').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":5 ,
        "ordering": false
    } );    
    } );
    $("#search_result_length").hide();
    $( function() {
      $("#from_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
    $( function() {
      $("#to_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
     $( function() {
      $("#expire_from").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
    $( function() {
      $("#expire_to").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
</script>